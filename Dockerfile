FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/appl

COPY . /usr/src/appl
RUN mvn package

ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
